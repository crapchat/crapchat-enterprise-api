package com.crapchatapp.model;

import java.time.LocalDateTime;

public class Crap {

    public Crap() {
    }

    public Crap(String uuid,
                int type,
                int size,
                String userId,
                String description,
                Coords coordinates,
                LocalDateTime created) {
        this.uuid = uuid;
        this.type = type;
        this.size = size;
        this.userId = userId;
        this.description = description;
        this.coordinates = coordinates;
        this.created = created;
    }


    private String uuid;
    private int type;
    private int size;
    private String userId;
    private String description;
    private Coords coordinates;
    private LocalDateTime created;

    public int getType() {
        return type;
    }

    public int getSize() {
        return size;
    }

    public String getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public Coords getCoordinates() {
        return coordinates;
    }

    public String getUuid() {
        return uuid;
    }

    public LocalDateTime getCreated() {
        return created;
    }

}
