package com.crapchatapp.model;

public class Coords {
    
    private double x;
    private double y;

    public Coords() {
    }

    public Coords(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
