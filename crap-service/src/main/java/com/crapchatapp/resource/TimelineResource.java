package com.crapchatapp.resource;

import com.crapchatapp.model.Crap;
import com.crapchatapp.service.CrapService;
import com.crapchatapp.service.TokenService;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Path("/timeline")
public class TimelineResource {

    private final TokenService tokenService;
    private final CrapService crapService;

    @Inject
    public TimelineResource(TokenService tokenService, CrapService crapService) {
        this.tokenService = tokenService;
        this.crapService = crapService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Crap> getTimeline(@Context HttpHeaders requestHeaders, @QueryParam("page") @DefaultValue("0") int page) {
        String userId = tokenService.getAccessToken(requestHeaders).sub;
        return crapService.getTimeline(userId, requestHeaders.getHeaderString("Authorization"), page);
    }
    
}
