package com.crapchatapp.resource;

import com.crapchatapp.model.Crap;
import com.crapchatapp.service.AuthorizationService;
import com.crapchatapp.service.CrapService;
import com.crapchatapp.service.TokenService;
import io.quarkus.security.UnauthorizedException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Base64;
import java.util.List;

@Path("/")
@RequestScoped
public class CrapResource {

    private final TokenService tokenService;
    private final CrapService crapService;
    private final AuthorizationService authorizationService;

    @Inject
    public CrapResource(TokenService tokenService, CrapService crapService, AuthorizationService authorizationService) {
        this.tokenService = tokenService;
        this.crapService = crapService;
        this.authorizationService = authorizationService;
    }

    @Path("craps")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response postCrap(Crap crap, @Context HttpHeaders requestHeaders) {
        //Todo: object validation
        Crap newCrap = crapService.createCrap(crap, tokenService.getAccessToken(requestHeaders).sub);
        return Response.created(URI.create("/craps/" + newCrap.getUuid())).build();
    }

    @Path("users/{userId}/craps")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public List<Crap> getCrapsByUserId(@Context HttpHeaders requestHeaders,
                                       @QueryParam("index") @DefaultValue(value = "0") int index,
                                       @QueryParam("size") @DefaultValue(value = "20") int size,
                                       @PathParam("userId") String userId) {
        // User requests own craps:
        String decodedUserId = new String(Base64.getDecoder().decode(userId));
        if (decodedUserId.equals(tokenService.getAccessToken(requestHeaders).sub)) {
            return crapService.getCrapsByUserId(decodedUserId, index, size);
        }
        // Check if the calling user is following the target user:
        if (!authorizationService.isFollowing(tokenService.getAccessToken(requestHeaders).sub,
                decodedUserId,
                requestHeaders.getHeaderString("Authorzation"))) {
            throw new UnauthorizedException("You are not following this user");
        }
        return crapService.getCrapsByUserId(decodedUserId, index, size);
    }

}