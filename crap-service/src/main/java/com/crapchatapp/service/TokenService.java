package com.crapchatapp.service;

import com.crapchatapp.model.AccessToken;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.HttpHeaders;
import java.util.Base64;

@ApplicationScoped
public class TokenService {

    private final Logger logger = LoggerFactory.getLogger(TokenService.class);

    public AccessToken getAccessToken(HttpHeaders request) throws NotAuthorizedException {
        String jwtToken = getJwt(request);
        if (jwtToken == null) {
            logger.warn("Request without auth header");
            throw new NotAuthorizedException("No token provided");
        }
        String idToken = new String(Base64.getDecoder().decode(jwtToken.split("\\.")[1]));
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        try {
            return objectMapper.readValue(idToken, AccessToken.Builder.class).build();
        } catch (JsonProcessingException e) {
            logger.warn("Invalid formatted token", e);
            throw new NotAuthorizedException("Invalid JSON token");
        }
    }

    public String getJwt(HttpHeaders request) throws NotAuthorizedException {
        String bearer = getBearer(request);
        if (!bearer.toLowerCase().startsWith("bearer ")) {
            logger.warn("Invalid formatted Authorization header {}", bearer);
            throw new NotAuthorizedException("Invalid authorization header");
        }
        return bearer.split(" ")[1];
    }

    public String getBearer(HttpHeaders request) throws NotAuthorizedException {
        String bearer = request.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (bearer == null) {
            throw new NotAuthorizedException("No token provided");
        }
        return bearer;
    }

    public String name(HttpHeaders httpHeaders) {
        AccessToken accessToken = getAccessToken(httpHeaders);
        if (accessToken.name != null && !accessToken.name.isBlank()) {
            return accessToken.name;
        }
        if (accessToken.preferredUsername != null
                && !accessToken.preferredUsername.isBlank()) {
            return accessToken.preferredUsername;
        }
        if (accessToken.email != null
                && !accessToken.email.isBlank()) {
            return accessToken.email;
        }
        return accessToken.sub;
    }


}
