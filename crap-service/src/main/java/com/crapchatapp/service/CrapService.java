package com.crapchatapp.service;

import com.crapchatapp.model.Crap;
import com.crapchatapp.model.Follower;
import com.crapchatapp.persistence.CrapEntity;
import com.crapchatapp.service.restclient.UserServiceClient;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class CrapService {

    private final UserServiceClient userServiceClient;

    @Inject
    public CrapService(@RestClient UserServiceClient userServiceClient) {
        this.userServiceClient = userServiceClient;
    }

    @Transactional
    public Crap createCrap(Crap crap, String userId) {
        CrapEntity crapEntity = new CrapEntity.Builder()
                .setUuid(UUID.randomUUID())
                .setUserId(userId)
                .setDescription(crap.getDescription())
                .setPosX(crap.getCoordinates().getX())
                .setPosY(crap.getCoordinates().getY())
                .setSize(crap.getSize())
                .setType(crap.getType())
                .build();
        crapEntity.persist();
        return crapEntity.toDto();
    }

    public List<Crap> getTimeline(String userId, String authorizationHeader, int page) {
        List<Follower> follows = userServiceClient.getFollowing(base64Encode(userId), authorizationHeader, null, true);
        //Todo: own craps should also be in timeline
        List<String> users = follows.stream()
                .map(Follower::getFollowingUserId)
                .collect(Collectors.toList());
        users.add(userId);
        return CrapEntity.<CrapEntity>find("userId IN (?1)", Sort.descending("created"), users)
                .page(Page.of(page, 25))
                .stream()
                .map(CrapEntity::toDto)
                .collect(Collectors.toList());
    }

    public List<Crap> getCrapsByUserId(String userId, int index, int size) {
        return CrapEntity.<CrapEntity>find("userId = ?1", Sort.descending("created"), userId)
                .page(Page.of(index, size))
                .stream()
                .map(CrapEntity::toDto)
                .collect(Collectors.toList());
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
}
