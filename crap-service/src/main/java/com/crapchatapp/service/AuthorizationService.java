package com.crapchatapp.service;

import com.crapchatapp.model.Follower;
import com.crapchatapp.service.restclient.UserServiceClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class AuthorizationService {

    private final UserServiceClient userServiceClient;

    @Inject
    public AuthorizationService(@RestClient UserServiceClient userServiceClient) {
        this.userServiceClient = userServiceClient;
    }

    public boolean isFollowing(String targetUserId, String followingUserId, String authorizationHeader) {
        List<Follower> follows = userServiceClient.getFollowers(base64Encode(targetUserId),
                authorizationHeader,
                base64Encode(followingUserId),
                true);
        return follows.size() > 0;
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
    
}
