package com.crapchatapp.persistence;

import com.crapchatapp.model.Coords;
import com.crapchatapp.model.Crap;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class CrapEntity extends PanacheEntity {

    public UUID uuid;
    public int type;
    public int size;
    public String userId;
    public String description;
    public double posX;
    public double posY;
    @CreationTimestamp
    public LocalDateTime created;


    public CrapEntity() {
    }

    private CrapEntity(Builder builder) {
        id = builder.id;
        uuid = builder.uuid;
        type = builder.type;
        size = builder.size;
        userId = builder.userId;
        description = builder.description;
        posX = builder.posX;
        posY = builder.posY;
        created = builder.created;
    }

    public Crap toDto() {
        return new Crap(uuid.toString(), type, size, userId, description, new Coords(posX, posY), created);
    }

    public static final class Builder {
        private Long id;
        private UUID uuid;
        private int type;
        private int size;
        private String userId;
        private String description;
        private double posX;
        private double posY;
        public LocalDateTime created;

        public Builder() {
        }

        public void setCreated(LocalDateTime created) {
            this.created = created;
        }
        
        public Builder setId(Long val) {
            id = val;
            return this;
        }

        public Builder setUuid(UUID val) {
            uuid = val;
            return this;
        }

        public Builder setType(int val) {
            type = val;
            return this;
        }

        public Builder setSize(int val) {
            size = val;
            return this;
        }

        public Builder setUserId(String val) {
            userId = val;
            return this;
        }

        public Builder setDescription(String val) {
            description = val;
            return this;
        }

        public Builder setPosX(double val) {
            posX = val;
            return this;
        }

        public Builder setPosY(double val) {
            posY = val;
            return this;
        }

        public CrapEntity build() {
            return new CrapEntity(this);
        }
    }
}
