package com.crapchatapp;

import com.crapchatapp.model.Coords;
import com.crapchatapp.model.Crap;
import com.crapchatapp.model.Follower;
import com.crapchatapp.util.FollowerStorage;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import static com.crapchatapp.util.JwtUtils.getJwt;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class CrapResourceTest {

    private final FollowerStorage followerStorage;

    @Inject
    public CrapResourceTest(FollowerStorage followerStorage) {
        this.followerStorage = followerStorage;
    }

    @BeforeEach
    public void clearStorage() {
        followerStorage.clearStorage();
    }

    @Test
    public void postCrap() {
        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(UUID.randomUUID().toString()))
                .body(new Crap(null, 5, 5, null, null, new Coords(420, 69), null))
                .post("/craps")
                .peek()
                .then()
                .statusCode(201);
    }

    @Test
    public void getOwnCrapsByUserId_30craps_200response2pages() {
        String userId = UUID.randomUUID().toString();
        String authToken = getJwt(userId);
        for (int i = 0; i < 30; i++) {
            given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + authToken)
                    .body(new Crap(null, 5, 5, null, String.valueOf(i), new Coords(420, 69), null))
                    .post("/craps")
                    .then()
                    .statusCode(201);
        }
        List<Crap> crapsResponse = Arrays.asList(given()
                .pathParam("userId", base64Encode(userId))
                .header("Authorization", "Bearer " + authToken)
                .get("/users/{userId}/craps")
                .peek()
                .as(Crap[].class));

        assertEquals(20, crapsResponse.size());

        List<Crap> crapsResponse2 = Arrays.asList(given()
                .pathParam("userId", base64Encode(userId))
                .queryParam("index", 1)
                .header("Authorization", "Bearer " + authToken)
                .get("/users/{userId}/craps")
                .peek()
                .as(Crap[].class));

        assertEquals(10, crapsResponse2.size());
    }

    @Test
    public void getCrapsFromOtherUser_notFollowing_unauthorizedException() {
        String followingUserId = UUID.randomUUID().toString();
        String targetUserId = UUID.randomUUID().toString();
        String authToken = getJwt(targetUserId);

        given()
                .pathParam("userId", base64Encode(followingUserId))
                .header("Authorization", "Bearer " + authToken)
                .get("/users/{userId}/craps")
                .peek()
                .then()
                .statusCode(401);
    }

    @Test
    public void getCrapsFromOtherUser_following_200responseEmptyArray() {
        String followingUserId = UUID.randomUUID().toString();
        String targetUserId = UUID.randomUUID().toString();
        followerStorage.addFollower(new Follower(targetUserId,
                followingUserId,
                true));
        String authToken = getJwt(targetUserId);

        given()
                .pathParam("userId", base64Encode(followingUserId))
                .header("Authorization", "Bearer " + authToken)
                .get("/users/{userId}/craps")
                .peek()
                .then()
                .statusCode(200);
    }

    @Test
    public void getCrapsFromOtherUser_followNotAccepted_401response() {
        String followingUserId = UUID.randomUUID().toString();
        String targetUserId = UUID.randomUUID().toString();
        followerStorage.addFollower(new Follower(targetUserId, followingUserId, false));
        String authToken = getJwt(targetUserId);

        given()
                .pathParam("userId", base64Encode(followingUserId))
                .header("Authorization", "Bearer " + authToken)
                .get("/users/{userId}/craps")
                .peek()
                .then()
                .statusCode(401);
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
}