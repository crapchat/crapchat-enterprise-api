package com.crapchatapp.util;

import com.crapchatapp.model.Follower;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class FollowerStorage {
    
    private List<Follower> followers;

    public FollowerStorage() {
        this.followers = new ArrayList<>();
    }

    public List<Follower> getFollowers() {
        return followers;
    }
    
    public void addFollower(Follower follow) {
        followers.add(follow);
    }
    
    public void clearStorage() {
        this.followers = new ArrayList<>();
    }
}
