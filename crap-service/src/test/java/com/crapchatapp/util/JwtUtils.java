package com.crapchatapp.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.UUID;

public class JwtUtils {

    public static String getJwt(String userId) {
        return JWT.create().withSubject(userId).sign(Algorithm.none());
    }
    
}
