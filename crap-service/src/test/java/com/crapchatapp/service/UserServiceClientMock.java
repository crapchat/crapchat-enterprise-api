package com.crapchatapp.service;

import com.crapchatapp.model.Follower;
import com.crapchatapp.util.FollowerStorage;
import io.quarkus.test.Mock;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mock
@ApplicationScoped
@RestClient
public class UserServiceClientMock implements com.crapchatapp.service.restclient.UserServiceClient {

    private final FollowerStorage followerStorage;

    @Inject
    public UserServiceClientMock(FollowerStorage followerStorage) {
        this.followerStorage = followerStorage;
    }

    @Override
    public List<Follower> getFollowers(String targetUserId, String authorizationHeader, String followingUserId, boolean accepted) {
        Stream<Follower> followers = followerStorage.getFollowers().stream()
                .filter(follow -> follow.isAccepted() == accepted);
        if (targetUserId != null) {
            followers = followers.filter(follow -> base64Encode(follow.getTargetUserId()).equals(targetUserId));
        }
        if (followingUserId != null) {
            followers = followers.filter(follow -> base64Encode(follow.getFollowingUserId()).equals(followingUserId));
        }

        return followers.collect(Collectors.toList());
    }

    @Override
    public List<Follower> getFollowing(String followingUserId, String authorizationHeader, String followedUserId, boolean accepted) {
        //Todo: mock not complete yet (copied from getFollowers)
        Stream<Follower> followers = followerStorage.getFollowers().stream()
                .filter(follow -> follow.isAccepted() == accepted);
        if (followingUserId != null) {
            followers = followers.filter(follow -> base64Encode(follow.getTargetUserId()).equals(followingUserId));
        }
        if (followedUserId != null) {
            followers = followers.filter(follow -> base64Encode(follow.getFollowingUserId()).equals(followedUserId));
        }

        return followers.collect(Collectors.toList());
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

}
