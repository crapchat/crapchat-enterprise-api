package com.crapchatapp;

import com.crapchatapp.util.FollowerStorage;
import com.crapchatapp.model.Coords;
import com.crapchatapp.model.Crap;
import com.crapchatapp.model.Follower;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.crapchatapp.util.JwtUtils.getJwt;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
public class TimelineResourceTest {

    private final FollowerStorage followerStorage;

    @Inject
    public TimelineResourceTest(FollowerStorage followerStorage) {
        this.followerStorage = followerStorage;
    }

    @BeforeEach
    public void clearStorage() {
        followerStorage.clearStorage();
    }

    @Test
    public void getEmptyTimeline() {
        String userId = UUID.randomUUID().toString();
        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(0, response.size());
    }

    @Test
    public void getTimeline_oneAcceptedFollower_oneCrap() {
        //The user that reads the timeline
        String followingUserId = UUID.randomUUID().toString();
        //The user that submits a crap
        String userId = UUID.randomUUID().toString();
        Crap crap = new Crap(null, 5, 6, null, "Testbeschrijving", new Coords(420, 69), null);
        followerStorage.addFollower(new Follower(followingUserId, userId, true));

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(crap)
                .post("/craps")
                .peek()
                .then()
                .statusCode(201);

        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(1, response.size());
        assertNotNull(response.get(0).getUuid());
        assertEquals(userId, response.get(0).getUserId());
        assertEquals(5, response.get(0).getType());
        assertEquals(6, response.get(0).getSize());
        assertEquals(420, response.get(0).getCoordinates().getX());
        assertEquals(69, response.get(0).getCoordinates().getY());
    }

    @Test
    public void getTimeline_selfPosted_oneCrap() {
        String userId = UUID.randomUUID().toString();
        Crap crap = new Crap(null, 5, 6, null, "Testbeschrijving", new Coords(420, 69), null);

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(crap)
                .post("/craps")
                .peek()
                .then()
                .statusCode(201);

        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(1, response.size());
        assertNotNull(response.get(0).getUuid());
        assertEquals(userId, response.get(0).getUserId());
        assertEquals(5, response.get(0).getType());
        assertEquals(6, response.get(0).getSize());
        assertEquals(420, response.get(0).getCoordinates().getX());
        assertEquals(69, response.get(0).getCoordinates().getY());
    }
    
    @Test
    public void getTimeline_oneUnacceptedFollower_emptyArray() {
        String followingUserId = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();
        Crap crap = new Crap(null, 5, 6, null, "Testbeschrijving", new Coords(420, 69), null);
        followerStorage.addFollower(new Follower(followingUserId, userId, false));

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .body(crap)
                .post("/craps")
                .peek()
                .then()
                .statusCode(201);

        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(0, response.size());
    }

    @Test
    public void getTimeline_pagination_multipleCraps() {
        //The user that reads the timeline
        String followingUserId = UUID.randomUUID().toString();
        //The user that submits a crap
        String userId = UUID.randomUUID().toString();
        Crap crap = new Crap(null, 5, 6, null, "Testbeschrijving", new Coords(420, 69), null);
        followerStorage.addFollower(new Follower(followingUserId, userId, true));

        for (int i = 0; i < 40; i++) {
            given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + getJwt(userId))
                    .body(crap)
                    .post("/craps")
                    .peek()
                    .then()
                    .statusCode(201);
        }

        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(25, response.size());
        
        List<Crap> response2 = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .queryParam("page", 1)
                .get("/timeline")
                .peek()
                .as(Crap[].class));
        
        assertEquals(15, response2.size());
    }

    @Test
    public void getTimeline_oneCrap_pagination() {
        //The user that reads the timeline
        String followingUserId = UUID.randomUUID().toString();
        //The user that submits a crap
        String userId = UUID.randomUUID().toString();
        Crap crap = new Crap(null, 5, 6, null, "Testbeschrijving", new Coords(420, 69), null);
        followerStorage.addFollower(new Follower(followingUserId, userId, true));

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(crap)
                .post("/craps")
                .peek()
                .then()
                .statusCode(201);

        List<Crap> response = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(1, response.size());

        List<Crap> response2 = Arrays.asList(given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .queryParam("page", 1)
                .get("/timeline")
                .peek()
                .as(Crap[].class));

        assertEquals(0, response2.size());
    }
}
