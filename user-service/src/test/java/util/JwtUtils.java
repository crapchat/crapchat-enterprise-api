package util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.UUID;

public class JwtUtils {

    public static String getJwt(UUID userId) {
        return JWT.create().withSubject(userId.toString()).sign(Algorithm.none());
    }
    
}
