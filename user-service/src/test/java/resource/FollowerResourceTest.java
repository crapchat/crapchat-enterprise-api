package resource;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import model.Follower;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.JwtUtils.getJwt;

@QuarkusTest
public class FollowerResourceTest {

    @Test
    public void addFollower_notAccepted() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", false)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(1, as.size());
        assertEquals(userId.toString(), as.get(0).getFollowingUserId());
        assertEquals(targetUserId.toString(), as.get(0).getTargetUserId());
    }

    @Test
    public void getFollowersOtherUser_notFollowing_unauthorizedStatusCode() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .queryParam("accepted", false)
                .get("/users/{userId}/followers")
                .then()
                .statusCode(401);
    }

    @Test
    public void acceptFollow_incomingFollow_accepted() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .pathParam("followingUserId", base64Encode(userId.toString()))
                .body(new Follower(null, null, true))
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .patch("/users/{userId}/followers/{followingUserId}")
                .then()
                .statusCode(204);

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", true)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(1, as.size());
        assertEquals(userId.toString(), as.get(0).getFollowingUserId());
        assertEquals(targetUserId.toString(), as.get(0).getTargetUserId());
    }


    @Test
    public void acceptFollow_differentUserId_unauthorizedResponseCode() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .pathParam("followingUserId", base64Encode(userId.toString()))
                .body(new Follower(null, null, true))
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(userId))
                .patch("/users/{userId}/followers/{followingUserId}")
                .then()
                .statusCode(401);
    }

    @Test
    public void followRequest_toSelf_ignore() {
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .post("/users/{userId}/followers")
                .peek()
                .then()
                .statusCode(204);

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", false)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(0, as.size());
    }

    @Test
    public void addFollower_alreadyRequested_idempotentResponse() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", false)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(1, as.size());
        assertEquals(userId.toString(), as.get(0).getFollowingUserId());
        assertEquals(targetUserId.toString(), as.get(0).getTargetUserId());
    }

    @Test
    public void getFollowers_multipleFollowersFilterResults_oneResult() {
        UUID targetUserId = UUID.randomUUID();
        List<String> followers = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            UUID tmpUserId = UUID.randomUUID();
            followers.add(tmpUserId.toString());
            given()
                    .pathParam("userId", base64Encode(targetUserId.toString()))
                    .header("Authorization", "Bearer " + getJwt(tmpUserId))
                    .post("/users/{userId}/followers")
                    .then()
                    .statusCode(204);
        }

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", false)
                .queryParam("followingUserId", followers.get(0))
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(1, as.size());
        assertEquals(followers.get(0), as.get(0).getFollowingUserId());
        assertEquals(targetUserId.toString(), as.get(0).getTargetUserId());
    }

    @Test
    public void getFollowers_multipleFollowers_allResults() {
        UUID targetUserId = UUID.randomUUID();
        int amountOfFollowers = 20;
        for (int i = 0; i < amountOfFollowers; i++) {
            UUID tmpUserId = UUID.randomUUID();
            given()
                    .pathParam("userId", base64Encode(targetUserId.toString()))
                    .header("Authorization", "Bearer " + getJwt(tmpUserId))
                    .post("/users/{userId}/followers")
                    .then()
                    .statusCode(204);
        }

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", false)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(amountOfFollowers, as.size());
    }

    @Test
    public void getFollowing_followingMultipleAccounts_allResults() {
        UUID followingUserId = UUID.randomUUID();
        int amountOfFollowing = 20;
        for (int i = 0; i < amountOfFollowing; i++) {
            UUID tmpUserId = UUID.randomUUID();
            given()
                    .pathParam("userId", base64Encode(tmpUserId.toString()))
                    .header("Authorization", "Bearer " + getJwt(followingUserId))
                    .post("/users/{userId}/followers")
                    .then()
                    .statusCode(204);
        }

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(followingUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .queryParam("accepted", false)
                .get("/users/{userId}/following")
                .peek()
                .as(Follower[].class));

        assertEquals(amountOfFollowing, as.size());
    }

    @Test
    public void deleteFollow_incomingFollow_accepted() {
        UUID userId = UUID.randomUUID();
        UUID targetUserId = UUID.randomUUID();
        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(userId))
                .post("/users/{userId}/followers")
                .then()
                .statusCode(204);

        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .pathParam("followingUserId", base64Encode(userId.toString()))
                .body(new Follower(null, null, true))
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .patch("/users/{userId}/followers/{followingUserId}")
                .then()
                .statusCode(204);

        List<Follower> as = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", true)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(1, as.size());

        given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .pathParam("followingUserId", base64Encode(userId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .delete("/users/{userId}/followers/{followingUserId}")
                .peek();

        List<Follower> as2 = Arrays.asList(given()
                .pathParam("userId", base64Encode(targetUserId.toString()))
                .header("Authorization", "Bearer " + getJwt(targetUserId))
                .queryParam("accepted", true)
                .get("/users/{userId}/followers")
                .peek()
                .as(Follower[].class));

        assertEquals(0, as2.size());
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
}
