package resource;

import io.quarkus.test.junit.QuarkusTest;
import model.User;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class UserResourceTest {
    //Todo: mock auth0 service

    @Test
    public void getUser_existingUser_200response() {
        String testUserId = "auth0|5e27543b74dfe10eaa478c95";
        User user = given()
                .pathParam("userId", base64Encode(testUserId))
                .get("/users/{userId}")
                .peek()
                .as(User.class);

        assertEquals(testUserId, user.getId());
    }

    @Test
    public void getUser_nonExistingUser_notFoundResponse() {
        String testUserId = base64Encode("auth0|6e27543b74dfe10eaa478c95");
        given()
                .pathParam("userId", testUserId)
                .get("/users/{userId}")
                .peek()
                .then()
                .statusCode(404);
    }

//    @Test
//    public void searchUser_noMatchingUsers_emptyArray() {
//        List<User> usersResult = Arrays.asList(given()
//                .queryParam("q", "")
//                .get("/users")
//                .peek()
//                .as(User[].class));
//
//        assertEquals(0, usersResult.size());
//    }

    @Test
    public void searchUser_niekQuery_oneResult() {
        List<User> usersResult = Arrays.asList(given()
                .queryParam("q", "niek")
                .get("/users")
                .peek()
                .as(User[].class));

        assertEquals(1, usersResult.size());
    }

    @Test
    public void searchUser_twoCharacters_badRequestResponse() {
        given()
                .queryParam("q", "ni")
                .get("/users")
                .peek()
                .then()
                .statusCode(400);
    }

    @Test
    public void searchUser_specialCharacters_badRequestResponse() {
        given()
                .queryParam("q", ":'_\"")
                .get("/users")
                .peek()
                .then()
                .statusCode(400);
    }

    private static String base64Encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

}
