package resource;

import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.UserService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Path("/")
public class UserResource {

    private final UserService userService;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }
    
    @Path("users/{userId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserById(@PathParam("userId") String userId) {
        String decodedUserId = new String(Base64.getDecoder().decode(userId));
        //Todo: Authorization
        try {
            return userService.getUserById(decodedUserId);
        } catch (APIException e) {
            throw new NotFoundException("User not found");
        } catch (Auth0Exception e) {
            throw new InternalServerErrorException();
        }
    }
    
    @Path("users")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> searchUser(@QueryParam("q") String query) {
        try {
            return userService.searchUser(query);
        } catch (Auth0Exception e) {
            logger.error("Auth0Exception", e);
            throw new InternalServerErrorException();
        }
    }
    
}
