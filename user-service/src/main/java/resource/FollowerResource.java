package resource;

import io.quarkus.security.UnauthorizedException;
import model.Follower;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.FollowerService;
import service.TokenService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

@Path("/")
public class FollowerResource {

    private final FollowerService followerService;
    private final TokenService tokenService;

    Logger logger = LoggerFactory.getLogger(FollowerResource.class);

    @Inject
    public FollowerResource(FollowerService followerService, TokenService tokenService) {
        this.followerService = followerService;
        this.tokenService = tokenService;
    }

    @GET
    @Path("users/{userId}/followers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Follower> getFollowers(@PathParam("userId") String userId,
                                       @QueryParam("accepted") @DefaultValue("true") boolean accepted,
                                       @QueryParam("followingUserId") String followingUserId,
                                       @Context HttpHeaders httpHeaders) {
        String decodedUserId = base64Decode(userId);
        String callingUserId = tokenService.getAccessToken(httpHeaders).sub;
        if (!followerService.isFollowing(callingUserId, decodedUserId) && !callingUserId.equals(decodedUserId)) {
            logger.info("User {} tried to get followers from {} but was unauthorized (not following)",
                    callingUserId,
                    userId);
            throw new NotAuthorizedException("Not following user");
        }
        return followerService.getFollowers(decodedUserId, accepted, followingUserId);
    }

    @Path("users/{userId}/followers")
    @POST
    public void addFollower(@PathParam("userId") String userId, @Context HttpHeaders httpHeaders) {
        String decodedUserId = base64Decode(userId);
        logger.info("POST /users/{}/followers", decodedUserId);
        String callingUserId = tokenService.getAccessToken(httpHeaders).sub;
        followerService.addFollower(callingUserId, decodedUserId, false);
    }

    @GET
    @Path("users/{userId}/following")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Follower> getFollowing(@PathParam("userId") String userId,
                                       @QueryParam("accepted") @DefaultValue("true") boolean accepted,
                                       @QueryParam("followedUserId") String followedUserId,
                                       @Context HttpHeaders httpHeaders) {
        String decodedUserId = base64Decode(userId);
        String decodedFollowedUserId = base64Decode(followedUserId);
        logger.info("Getting users that {} follows", decodedUserId);
        String callingUserId = tokenService.getAccessToken(httpHeaders).sub;
        logger.info("{}.equals({}) = {}", callingUserId, decodedUserId, callingUserId.equals(decodedUserId));
        logger.info("callingUserId: {} | decodedUserId: {}",
                callingUserId.getBytes(StandardCharsets.UTF_8),
                decodedUserId.getBytes(StandardCharsets.UTF_8));
        if (callingUserId.equals(decodedUserId)) {
            logger.info("User {} requested following users for itself", callingUserId);
            return followerService.getFollowing(decodedUserId, accepted, decodedFollowedUserId);
        }
        if (!followerService.isFollowing(callingUserId, decodedUserId)) {
            logger.info("User {} tried to get following users from {} but was unauthorized (not following)",
                    callingUserId,
                    decodedUserId);
            throw new NotAuthorizedException("Not following user");
        }
        return followerService.getFollowing(decodedUserId, accepted, decodedFollowedUserId);
    }

    @Path("users/{userId}/followers/{followingUserId}")
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    public void editFollower(@PathParam("userId") String targetUserId,
                             @PathParam("followingUserId") String followingUserId,
                             @Context HttpHeaders httpHeaders,
                             Follower follower) {
        String decodedTargetUserId = base64Decode(targetUserId);
        String decodedFollowingUserId = base64Decode(followingUserId);
        String callingUserId = tokenService.getAccessToken(httpHeaders).sub;
        if (!callingUserId.equals(decodedTargetUserId)) {
            logger.info("User {} tried to edit a follow that was not theirs", callingUserId);
            throw new UnauthorizedException("Only the targetUser can edit (or accept) a follow request");
        }
        followerService.editFollower(decodedTargetUserId, decodedFollowingUserId, follower);
    }

    @Path("users/{userId}/followers/{followingUserId}")
    @DELETE
    public void deleteFollower(@PathParam("userId") String targetUserId,
                               @PathParam("followingUserId") String followingUserId,
                               @Context HttpHeaders httpHeaders
    ) {
        String decodedTargetUserId = base64Decode(targetUserId);
        String decodedFollowingUserId = base64Decode(followingUserId);
        String callingUserId = tokenService.getAccessToken(httpHeaders).sub;
        if (decodedTargetUserId.equals(callingUserId) || decodedFollowingUserId.equals(callingUserId)) {
            followerService.deleteFollower(decodedTargetUserId, decodedFollowingUserId);
            return;
        }
        throw new NotAuthorizedException("Not authorized");
    }

    private String base64Decode(String userId) {
        // hack to use special characters in the urls: https://github.com/quarkusio/quarkus/issues/17341
        if (userId == null) {
            return null;
        }
        return new String(Base64.getDecoder().decode(userId));
    }

}
