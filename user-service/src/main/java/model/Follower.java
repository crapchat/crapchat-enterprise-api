package model;

public class Follower {
    
    private String followingUserId;
    private String targetUserId;
    private boolean accepted;

    public Follower(String followingUserId, String targetUserId, boolean accepted) {
        this.followingUserId = followingUserId;
        this.targetUserId = targetUserId;
        this.accepted = accepted;
    }

    public Follower() {
    }

    public String getFollowingUserId() {
        return followingUserId;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public boolean isAccepted() {
        return accepted;
    }
}
