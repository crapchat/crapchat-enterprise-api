package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class AccessToken {

    public final int exp;
    public final int iat;
    public final int authTime;
    public final String jti;
    public final String iss;
    public final String sub;
    public final String typ;
    public final String azp;
    public final String[] aud;
    public final String nonce;
    public final String sessionState;
    public final String acr;
    public final List<String> allowedOrigins = new ArrayList<>();
    public final RealmAccess realmAccess;
    public final String scope;
    public final boolean emailVerified;
    public final String realm;
    public final String preferredUsername;
    public final String name;
    public final String email;
    public final String locale;

    private AccessToken(Builder b) {
        this.exp = b.exp;
        this.iat = b.iat;
        this.authTime = b.authTime;
        this.jti = b.jti;
        this.iss = b.iss;
        this.sub = b.sub;
        this.typ = b.typ;
        this.azp = b.azp;
        this.aud = b.aud;
        this.nonce = b.nonce;
        this.sessionState = b.sessionState;
        this.acr = b.acr;
        this.allowedOrigins.addAll(b.allowedOrigins);
        this.realmAccess = b.realmAccess;
        this.scope = b.scope;
        this.emailVerified = b.emailVerified;
        this.realm = b.realm;
        this.preferredUsername = b.preferredUsername;
        this.name = b.name;
        this.email = b.email;
        this.locale = b.locale;
    }

    public static class RealmAccess {
        public List<String> roles = new ArrayList<>();

        @Override
        public String toString() {
            return "RealmAccess{"
                    + "roles=" + roles
                    + '}';
        }

        public boolean isUserInRole(String role) {
            return roles.stream()
                    .anyMatch(r -> r.trim().equalsIgnoreCase(role.trim()));
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Builder {
        public int exp;
        public int iat;
        public int authTime;
        public String jti;
        public String iss;
        public String sub;
        public String typ;
        public String[] aud;
        public String azp;
        public String nonce;
        public String sessionState;
        public String acr;
        @JsonProperty("allowed-origins")
        public List<String> allowedOrigins = new ArrayList<>();
        public RealmAccess realmAccess;
        public String scope;
        public boolean emailVerified;
        public String realm;
        public String preferredUsername;
        public String name;
        public String email;
        public String locale;

        public AccessToken build() {
            return new AccessToken(this);
        }
    }

    @Override
    public String toString() {
        return "AccessToken{"
                + "exp=" + exp
                + ", iat=" + iat
                + ", authTime=" + authTime
                + ", jti='" + jti + '\''
                + ", iss='" + iss + '\''
                + ", sub='" + sub + '\''
                + ", typ='" + typ + '\''
                + ", azp='" + azp + '\''
                + ", aud='" + Arrays.toString(aud) + '\''
                + ", nonce='" + nonce + '\''
                + ", sessionState='" + sessionState + '\''
                + ", acr='" + acr + '\''
                + ", allowedOrigins=" + allowedOrigins
                + ", realmAccess=" + realmAccess
                + ", scope='" + scope + '\''
                + ", emailVerified=" + emailVerified
                + ", realm='" + realm + '\''
                + ", preferredUsername='" + preferredUsername + '\''
                + ", name='" + name + '\''
                + ", email='" + email + '\''
                + ", locale='" + locale + '\''
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccessToken)) {
            return false;
        }
        AccessToken that = (AccessToken) o;
        return exp == that.exp
                && iat == that.iat
                && authTime == that.authTime
                && emailVerified == that.emailVerified
                && Objects.equals(jti, that.jti)
                && Objects.equals(iss, that.iss)
                && Objects.equals(sub, that.sub)
                && Objects.equals(typ, that.typ)
                && Objects.equals(azp, that.azp)
                && Arrays.equals(aud, that.aud)
                && Objects.equals(nonce, that.nonce)
                && Objects.equals(sessionState, that.sessionState)
                && Objects.equals(acr, that.acr)
                && Objects.equals(allowedOrigins, that.allowedOrigins)
                && Objects.equals(realmAccess, that.realmAccess)
                && Objects.equals(scope, that.scope)
                && Objects.equals(realm, that.realm)
                && Objects.equals(preferredUsername, that.preferredUsername)
                && Objects.equals(name, that.name)
                && Objects.equals(email, that.email)
                && Objects.equals(locale, that.locale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exp, iat, authTime, jti, iss, sub, typ, azp, Arrays.hashCode(aud), nonce, sessionState, acr, allowedOrigins,
                realmAccess, scope, emailVerified, realm, preferredUsername, name, email, locale);
    }


}
