package model;

public class User {
    
    private String id;
    private String username;
    private String emailAddress;
    private String profileImage;

    public User() {
    }

    public User(String id, String username, String emailAddress, String profileImage) {
        this.id = id;
        this.username = username;
        this.emailAddress = emailAddress;
        this.profileImage = profileImage;
    }

    private User(Builder builder) {
        id = builder.id;
        username = builder.username;
        emailAddress = builder.emailAddress;
        profileImage = builder.profileImage;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public static final class Builder {
        private String id;
        private String username;
        private String emailAddress;
        private String profileImage;

        public Builder() {
        }

        public Builder setId(String val) {
            id = val;
            return this;
        }

        public Builder setUsername(String val) {
            username = val;
            return this;
        }

        public Builder setEmailAddress(String val) {
            emailAddress = val;
            return this;
        }

        public Builder setProfileImage(String val) {
            profileImage = val;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
