package service;

import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.exception.Auth0Exception;
import model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class UserService {

    private final Auth0Service auth0Service;

    private static final String DANGEROUS_QUERY_CHARACTERS = "[^a-zA-Z0-9, .]";

    @Inject
    public UserService(Auth0Service auth0Service) throws Auth0Exception {
        this.auth0Service = auth0Service;
    }

    public List<User> searchUser(String query) throws Auth0Exception {
        //todo: filter own account from search results
        String safeQuery = query.replaceAll(DANGEROUS_QUERY_CHARACTERS, "");
        if (safeQuery.length() < 3) {
            throw new BadRequestException("Search query must have at least 3 characters");
        }
        return auth0Service.searchUser(query).getItems().stream()
                .map(user -> new User(user.getId(), user.getUsername(), "", user.getPicture()))
                .collect(Collectors.toList());
    }


    public User getUserById(String userId) throws Auth0Exception {
        return auth0Service.getUserById(userId);
    }
}
