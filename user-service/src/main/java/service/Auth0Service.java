package service;

import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.mgmt.users.UsersPage;
import model.User;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class Auth0Service {

    private final ManagementAPI managementApi;
    
    @Inject
    public Auth0Service(@ConfigProperty(name = "auth0.domain") String auth0Domain,
                        @ConfigProperty(name = "auth0.clientId") String auth0ClientId,
                        @ConfigProperty(name = "auth0.clientSecret") String auth0ClientSecret) throws Auth0Exception {
        AuthAPI auth = new AuthAPI(auth0Domain, auth0ClientId, auth0ClientSecret);
        auth.doNotSendTelemetry();
        this.managementApi = new ManagementAPI(auth0Domain, auth.requestToken(auth0Domain + "/api/v2/").execute().getAccessToken());
    }

    public User getUserById(String userId) throws Auth0Exception {
        com.auth0.json.mgmt.users.User execute = managementApi.users().get(userId, null).execute();
        return new User.Builder()
                .setEmailAddress(execute.getEmail())
                .setProfileImage(execute.getPicture())
                .setId(execute.getId())
                .setUsername(execute.getUsername())
                .build();
    }

    /**
     * @param query String to search
     * @return Searches users by their name or nickname. The query string should not contain the following characters:
     * :_*,"'[]{}
     * @throws Auth0Exception
     */
    public UsersPage searchUser(String query) throws Auth0Exception {
        UserFilter userFilter = new UserFilter();
        userFilter.withQuery("name:*" + query + "* OR nickname:*" + query + "*");
        return managementApi.users().list(userFilter).execute();
    }
}
