package service;

import model.Follower;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persitence.FollowerEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class FollowerService {

    Logger logger = LoggerFactory.getLogger(Follower.class);

    @Transactional
    public void addFollower(String followingUserId, String targetUserId, boolean accepted) {
        if (followingUserId.equals(targetUserId)) {
            // Don't follow yourself lol
            return;
        }
        FollowerEntity followerEntity = new FollowerEntity.Builder()
                .setFollowerId(new FollowerEntity.FollowerId(followingUserId, targetUserId))
                .setAccepted(accepted)
                .build();
        try {
            followerEntity.persistAndFlush();
        } catch (PersistenceException e) {
            logger.info("Follower with targetUserId: {} and followingUserId: {} already exists",
                    targetUserId,
                    followingUserId);
            return;
        }
        logger.info("Added follow request for targetUserId: {}, followingUserId: {}",
                targetUserId,
                followingUserId);
    }

    public List<Follower> getFollowers(String targetUserId, boolean accepted, String followingUserId) {
        logger.info("Getting followers of targetUserId: {}", targetUserId);
        if (followingUserId != null) {
            return FollowerEntity.<FollowerEntity>stream("followerId.targetUserId = ?1 " +
                            "and accepted = ?2 " +
                            "and followerId.followingUserId = ?3",
                    targetUserId,
                    accepted,
                    followingUserId)
                    .map(FollowerEntity::toDto)
                    .collect(Collectors.toList());
        }
        return FollowerEntity.<FollowerEntity>stream("followerId.targetUserId = ?1 and accepted = ?2",
                targetUserId,
                accepted)
                .map(FollowerEntity::toDto)
                .collect(Collectors.toList());
    }

    public boolean isFollowing(String followingUserId, String targetUserId) {
        return FollowerEntity.count("followerId.targetUserId = ?1 " +
                        "and followerId.followingUserId = ?2 " +
                        "and accepted = ?3",
                targetUserId,
                followingUserId,
                true) > 0;
    }

    @Transactional
    public void editFollower(String targetUserId, String followingUserId, Follower follower) {
        FollowerEntity followerEntity = FollowerEntity.findById(new FollowerEntity.FollowerId(followingUserId, targetUserId));
        if (followerEntity == null) {
            throw new NotFoundException("Follow request not found");
        }
        followerEntity.accepted = follower.isAccepted();
        followerEntity.persist();
    }

    public List<Follower> getFollowing(String followingUserId, boolean accepted, String followedUserId) {
        logger.info("getFollowing. FollowingUserId: {}", followingUserId);
        if (followedUserId != null) {
            return FollowerEntity.<FollowerEntity>stream("followerId.followingUserId = ?1 " +
                            "and accepted = ?2 " +
                            "and followerId.targetUserId = ?3",
                    followingUserId,
                    accepted,
                    followedUserId)
                    .map(FollowerEntity::toDto)
                    .collect(Collectors.toList());
        }
        return FollowerEntity.<FollowerEntity>stream("followerId.followingUserId = ?1 and accepted = ?2",
                followingUserId,
                accepted)
                .map(FollowerEntity::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteFollower(String targetUserId, String followingUserId) {
        FollowerEntity.delete("followerId.targetUserId = ?1 and followerId.followingUserId = ?2", targetUserId, followingUserId);
    }
}
