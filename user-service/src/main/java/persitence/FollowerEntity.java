package persitence;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import model.Follower;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class FollowerEntity extends PanacheEntityBase {
    
    @EmbeddedId
    public FollowerId followerId;
    
    public boolean accepted;

    public FollowerEntity() {
    }

    private FollowerEntity(Builder builder) {
        followerId = builder.followerId;
        accepted = builder.accepted;
    }

    public Follower toDto() {
        return new Follower(followerId.followingUserId, followerId.targetUserId, accepted);
    }

    @Embeddable
    public static class FollowerId implements Serializable {
        public String followingUserId;
        public String targetUserId;

        public FollowerId(String followingUserId, String targetUserId) {
            this.followingUserId = followingUserId;
            this.targetUserId = targetUserId;
        }

        public FollowerId() {
        }
    }

    public static final class Builder {
        private FollowerId followerId;
        private boolean accepted;

        public Builder() {
        }

        public Builder setFollowerId(FollowerId val) {
            followerId = val;
            return this;
        }

        public Builder setAccepted(boolean val) {
            accepted = val;
            return this;
        }

        public FollowerEntity build() {
            return new FollowerEntity(this);
        }
    }
}
