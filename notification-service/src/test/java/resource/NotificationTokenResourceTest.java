package resource;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import model.NotificationToken;
import org.junit.jupiter.api.Test;
import persistence.NotificationTokenEntity;

import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static util.JwtUtils.getJwt;

@QuarkusTest
public class NotificationTokenResourceTest {

    @Test
    public void postNotificationToken_200response() {
        UUID userId = UUID.randomUUID();
        given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(new NotificationToken(UUID.randomUUID().toString(), null, null))
                .contentType(ContentType.JSON)
                .post("/notification-tokens")
                .peek()
                .then()
                .statusCode(204);
        List<NotificationTokenEntity> list = NotificationTokenEntity.list("userId = ?1", userId.toString());
        assertEquals(1, list.size());
        assertEquals(userId.toString(), list.get(0).userId);
        assertNotNull(list.get(0).uuid);
    }

    @Test
    public void postNotificationToken_alreadyExists_200response() {
        UUID userId = UUID.randomUUID();
        String pushToken = UUID.randomUUID().toString();
        given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(new NotificationToken(pushToken, null, null))
                .contentType(ContentType.JSON)
                .post("/notification-tokens")
                .peek()
                .then()
                .statusCode(204);
        given()
                .header("Authorization", "Bearer " + getJwt(userId))
                .body(new NotificationToken(pushToken, null, null))
                .contentType(ContentType.JSON)
                .post("/notification-tokens")
                .peek()
                .then()
                .statusCode(204);
        List<NotificationTokenEntity> list = NotificationTokenEntity.list("userId = ?1", userId.toString());
        assertEquals(1, list.size());
        assertEquals(userId.toString(), list.get(0).userId);
        assertNotNull(list.get(0).uuid);
    }

}
