package service;

import io.quarkus.test.Mock;
import model.Follower;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import service.restclient.UserServiceClient;
import util.FollowerStorage;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mock
@ApplicationScoped
@RestClient
public class UserServiceClientMock implements UserServiceClient {

    private final FollowerStorage followerStorage;

    @Inject
    public UserServiceClientMock(FollowerStorage followerStorage) {
        this.followerStorage = followerStorage;
    }

    @Override
    public List<Follower> getFollowers(String targetUserId, String authorizationHeader, String followingUserId, boolean accepted) {
        Stream<Follower> followers = followerStorage.getFollowers().stream()
                .filter(follow -> follow.isAccepted() == accepted);
        if (targetUserId != null) {
            followers = followers.filter(follow -> follow.getTargetUserId().equals(targetUserId));
        }
        if (followingUserId != null) {
            followers = followers.filter(follow -> follow.getFollowingUserId().equals(followingUserId));
        }

        return followers.collect(Collectors.toList());
    }

    @Override
    public List<Follower> getFollowing(String targetUserId, String authorizationHeader, String followingUserId, boolean accepted) {
        //Todo: mock not complete yet (copied from getFollowers)
        Stream<Follower> followers = followerStorage.getFollowers().stream()
                .filter(follow -> follow.isAccepted() == accepted);
        if (targetUserId != null) {
            followers = followers.filter(follow -> follow.getFollowingUserId().equals(targetUserId));
        }
        if (followingUserId != null) {
            followers = followers.filter(follow -> follow.getTargetUserId().equals(followingUserId));
        }

        return followers.collect(Collectors.toList());
    }

}
