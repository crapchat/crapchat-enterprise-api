package service;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import model.BroadcastNotificationMessage;
import model.Follower;
import model.NotificationToken;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.junit.ClassRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import util.FollowerStorage;

import javax.inject.Inject;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static io.restassured.RestAssured.given;
import static util.JwtUtils.getJwt;

@QuarkusTest
public class NotificationMessageConsumerTest {

    @ClassRule
    public static KafkaContainer KAFKA = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));

    static {
        // Testcontainers is not used in the Gitlab CI
        if (System.getenv("KAFKA_BOOTSTRAP_SERVERS") == null) {
            System.out.println("Starting Kafka Testcontainer. Make sure that the Docker daemon is available.");
            KAFKA.start();
            System.setProperty("kafka.bootstrap.servers", KAFKA.getBootstrapServers());
        }
    }

    @Inject
    public NotificationMessageConsumerTest(@Channel("test-broadcast-notification") Emitter<BroadcastNotificationMessage> broadcastNotificationMessageEmitter,
                                           FollowerStorage followerStorage) {
        this.broadcastNotificationMessageEmitter = broadcastNotificationMessageEmitter;
        this.followerStorage = followerStorage;
    }

    Emitter<BroadcastNotificationMessage> broadcastNotificationMessageEmitter;
    private final FollowerStorage followerStorage;

    @Test
    public void pushNotificationBroacast_noFollowers_noNotifications() {
        BroadcastNotificationMessage broadcastNotificationMessage =
                new BroadcastNotificationMessage(UUID.randomUUID().toString(),
                        UUID.randomUUID().toString(),
                        "Bearer " + getJwt(UUID.randomUUID()));
        CompletionStage<Void> send = broadcastNotificationMessageEmitter.send(broadcastNotificationMessage);
        Assertions.assertDoesNotThrow(() -> {
            send.toCompletableFuture().join();
        });
    }

    @Test
    public void pushNotificationBroadcast_oneFollowerWithoutPushToken_noNotification() {
        String targetUserId = UUID.randomUUID().toString();
        followerStorage.addFollower(new Follower(targetUserId, UUID.randomUUID().toString(), true));
        BroadcastNotificationMessage broadcastNotificationMessage =
                new BroadcastNotificationMessage(targetUserId,
                        UUID.randomUUID().toString(),
                        "Bearer " + getJwt(UUID.randomUUID()));
        CompletionStage<Void> send = broadcastNotificationMessageEmitter.send(broadcastNotificationMessage);
        Assertions.assertDoesNotThrow(() -> {
            send.toCompletableFuture().get();
        });
    }
    
    @Test
    public void pushNotificationBroadcast_oneFollowerWithOnePushToken_sendNotification() {
        UUID targetUserId = UUID.randomUUID();
        UUID followingUserId = UUID.randomUUID();
        followerStorage.addFollower(new Follower(targetUserId.toString(), followingUserId.toString(), true));
        given()
                .header("Authorization", "Bearer " + getJwt(followingUserId))
                .body(new NotificationToken(UUID.randomUUID().toString(), null, null))
                .contentType(ContentType.JSON)
                .post("/notification-tokens")
                .peek();
        CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).join();
        BroadcastNotificationMessage broadcastNotificationMessage =
                new BroadcastNotificationMessage(targetUserId.toString(),
                        UUID.randomUUID().toString(),
                        "Bearer " + getJwt(targetUserId));
        CompletionStage<Void> send = broadcastNotificationMessageEmitter.send(broadcastNotificationMessage);
        Assertions.assertDoesNotThrow(() -> {
            send.toCompletableFuture().get();
        });
        CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).join();
    }
    
}