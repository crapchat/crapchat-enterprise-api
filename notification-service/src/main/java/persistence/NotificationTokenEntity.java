package persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import model.NotificationToken;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
public class NotificationTokenEntity extends PanacheEntity {
    
    public UUID uuid;
    
    @Column(unique = true)
    public String token;
    
    public String userId;
    
    public NotificationToken toDto() {
        return new NotificationToken(token, userId, uuid);
    }
    
}
