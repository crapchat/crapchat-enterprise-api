package resource;

import model.NotificationToken;
import service.NotificationTokenService;
import service.TokenService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@Path("/notification-tokens")
public class NotificationTokenResource {

    private final NotificationTokenService notificationTokenService;
    private final TokenService tokenService;

    public NotificationTokenResource(NotificationTokenService notificationTokenService, TokenService tokenService) {
        this.notificationTokenService = notificationTokenService;
        this.tokenService = tokenService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void storeNotificationToken(@Context HttpHeaders httpHeaders, 
                                       NotificationToken notificationToken) {
        notificationTokenService.storeNotificationToken(notificationToken, 
                tokenService.getAccessToken(httpHeaders).sub);
    }
    
}
