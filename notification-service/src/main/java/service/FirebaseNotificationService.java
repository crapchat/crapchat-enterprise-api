package service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@ApplicationScoped
public class FirebaseNotificationService implements PushNotificationService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private FirebaseMessaging firebaseMessaging;

    @Inject
    public FirebaseNotificationService(@ConfigProperty(name = "firebase.serviceAccount") String serviceAccount) {
        byte[] decode = Base64.getDecoder().decode(serviceAccount);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decode);

        FirebaseOptions options = null;
        try {
            options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(byteArrayInputStream))
                    .build();
        } catch (IOException e) {
            logger.error("Failed to initiate Firebase client", e);
        }
        assert options != null;
        FirebaseApp.initializeApp(options);
        firebaseMessaging = FirebaseMessaging.getInstance();
    }

    public void sendNotification(String message, List<String> tokens) {
        if (firebaseMessaging == null) {
            firebaseMessaging = FirebaseMessaging.getInstance();
        }
        MulticastMessage multicastMessage = MulticastMessage.builder()
                .addAllTokens(tokens)
                .setNotification(Notification.builder()
                        .setBody(message)
                        .setTitle("Crapchat 💩")
                        .build())
                .build();
        try {
            firebaseMessaging.sendMulticast(multicastMessage);
        } catch (FirebaseMessagingException e) {
            logger.error("Failed to send Firebase message", e);
        }
    }

}
