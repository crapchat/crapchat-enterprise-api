package service;

import com.google.firebase.messaging.FirebaseMessagingException;
import io.smallrye.common.annotation.Blocking;
import model.BroadcastNotificationMessage;
import model.Follower;
import org.eclipse.microprofile.reactive.messaging.Acknowledgment;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.NotificationTokenEntity;
import service.restclient.UserServiceClient;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

@ApplicationScoped
public class NotificationMessageConsumer {

    private final PushNotificationService pushNotificationService;
    private final UserServiceClient userServiceClient;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public NotificationMessageConsumer(PushNotificationService pushNotificationService,
                                       @RestClient UserServiceClient userServiceClient) {
        this.pushNotificationService = pushNotificationService;
        this.userServiceClient = userServiceClient;
    }

    @Incoming("broadcast-notification")
    @Acknowledgment(Acknowledgment.Strategy.MANUAL)
    @Blocking
    @Transactional
    public CompletionStage<Void> sendBroadcastNotification(Message<BroadcastNotificationMessage> broadcastNotificationMessage) {
        BroadcastNotificationMessage payload = broadcastNotificationMessage.getPayload();
        logger.info("Sending notification: {}", payload.toString());
        List<Follower> followers = userServiceClient.getFollowers(payload.getUserId(),
                payload.getAuthorizationHeader(),
                null,
                true);
        if (followers.size() <= 0) {
            logger.info("No followers found. Not sending notifications.");
            return broadcastNotificationMessage.ack();
        }
        List<String> followerIds = followers.stream().map(Follower::getFollowingUserId).collect(Collectors.toList());
        List<String> pushTokens = NotificationTokenEntity
                .<NotificationTokenEntity>stream("userId IN (?1)", followerIds)
                .map(notificationTokenEntity -> notificationTokenEntity.userId)
                .collect(Collectors.toList());
        if (pushTokens.size() <= 0) {
            logger.info("No push tokens found. Not sending notifications.");
            return broadcastNotificationMessage.ack();
        }
        try {
            pushNotificationService.sendNotification(payload.getMessage(), pushTokens);
        } catch (FirebaseMessagingException e) {
            logger.error("Failed to send push notifications", e);
            broadcastNotificationMessage.nack(e);
        }
        return broadcastNotificationMessage.ack();
    }


}
