package service.restclient;

import model.Follower;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.util.List;


//@Path("/users")
@RegisterRestClient(configKey = "user-service")
public interface UserServiceClient {

    @GET
    @Path("/{userId}/followers")
    List<Follower> getFollowers(@PathParam("userId") String targetUserId,
                                @HeaderParam("Authorization") String authorizationHeader,
                                @QueryParam("followingUserId") String followingUserId,
                                @QueryParam("accepted") boolean accepted);
    
    @GET
    @Path("/{userId}/following")
    List<Follower> getFollowing(@PathParam("userId") String targetUserId,
                                @HeaderParam("Authorization") String authorizationHeader,
                                @QueryParam("followingUserId") String followingUserId,
                                @QueryParam("accepted") boolean accepted);

}
