package service;

import com.google.firebase.messaging.FirebaseMessagingException;

import java.util.List;

public interface PushNotificationService {
    
    void sendNotification(String message, List<String> tokens) throws FirebaseMessagingException;
    
}
