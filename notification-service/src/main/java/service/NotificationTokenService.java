package service;

import model.NotificationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.NotificationTokenEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.UUID;

@ApplicationScoped
public class NotificationTokenService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Transactional
    public void storeNotificationToken(NotificationToken notificationToken, String userId) {
        NotificationTokenEntity notificationTokenEntity = new NotificationTokenEntity();
        notificationTokenEntity.uuid = UUID.randomUUID();
        notificationTokenEntity.token = notificationToken.getToken();
        notificationTokenEntity.userId = userId;
        try { 
            notificationTokenEntity.persistAndFlush(); 
        } catch (PersistenceException e) {
            logger.info("User {} tried to register a new notification token which already exists", userId);
        }
    }
    
}
