package service;

import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;
import model.BroadcastNotificationMessage;

public class BroacastNotificationMessageDeserializer extends ObjectMapperDeserializer<BroadcastNotificationMessage> {
    public BroacastNotificationMessageDeserializer() {
        super(BroadcastNotificationMessage.class);
    }
}
