package model;

import java.util.UUID;

public class NotificationToken {
    
    private UUID uuid;
    private String token;
    private String userId;

    public NotificationToken() {
    }

    public NotificationToken(String token, String userId, UUID uuid) {
        this.token = token;
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }
}
