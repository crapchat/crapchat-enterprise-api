package model;

public class Follower {
    
    private String targetUserId;
    private String followingUserId;
    private boolean accepted;

    public Follower() {
    }

    public Follower(String targetUserId, String followingUserId, boolean accepted) {
        this.targetUserId = targetUserId;
        this.followingUserId = followingUserId;
        this.accepted = accepted;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public String getFollowingUserId() {
        return followingUserId;
    }

    public boolean isAccepted() {
        return accepted;
    }
}
