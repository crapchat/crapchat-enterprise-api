package model;

public class BroadcastNotificationMessage {
    
    private String userId;
    private String message;
    private String authorizationHeader;


    public BroadcastNotificationMessage() { }

    public BroadcastNotificationMessage(String userId, String message, String authorizationHeader) {
        this.userId = userId;
        this.message = message;
        this.authorizationHeader = authorizationHeader;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthorizationHeader() {
        return authorizationHeader;
    }

    @Override
    public String toString() {
        // Authorization header should not be printed
        return "BroadcastNotificationMessage{" +
                "userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
